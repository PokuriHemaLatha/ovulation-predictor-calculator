<?php
namespace Drupal\calculator_block\Plugin\Block;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
/**
 * Provides a BornGroup with a simple text.
 *
 * @Block(
 *   id = "calculator_block",
 *   admin_label = @Translation("CalculatorBlock"),
 * )
 */
class CalculatorBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
   
    return [
      '#markup' => $this->t('This is a calculator block!'),
    ];
  }
  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['CalculatorBlock_settings'] = $form_state->getValue('CalculatorBlock_settings');
  }
}