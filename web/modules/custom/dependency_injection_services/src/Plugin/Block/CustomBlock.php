<?php
/**
 * @file
 * Contains \Drupal\customblock\Plugin\Block\ArticleBlock.
 */

namespace Drupal\calculator_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Provides a 'Custom' block.
 *
 * @Block(
 *   id = "calculator_block",
 *   admin_label = @Translation("calculator block"),
 *   category = @Translation("Custom Custom block example")
 * )
 */
class CustomBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $data = \Drupal::service('calculator_block.dbinsert')->getData();
    return [
      '#theme' => 'my_template',
      '#data' => $data,
    ];
   }
}